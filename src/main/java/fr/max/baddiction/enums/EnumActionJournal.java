
package fr.max.baddiction.enums;

public enum EnumActionJournal {

	INSERT("INSERT"), UPDATE("UPDATE"), DELETE("DELETE");

	/** The code. */
	private final String code;

	/**
	 * EnumSexe.
	 * 
	 * @param code char
	 */
	private EnumActionJournal(final String code) {
		this.code = code;
	}

	/**
	 * Gets the code.
	 * 
	 * @return String code
	 */
	public String getCode() {
		return code;
	}

	@Override
	public String toString() {
		return name();
	}

}
