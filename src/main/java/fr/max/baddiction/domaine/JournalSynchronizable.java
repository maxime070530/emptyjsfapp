
package fr.max.baddiction.domaine;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import fr.max.baddiction.domaine.JournalSynchronizable.JournalKey;
import fr.max.baddiction.enums.EnumActionJournal;

/**
 * @author kcamroux
 */
@Entity
@Table(name = "journal_synchronizable")
public class JournalSynchronizable extends AbstractId<JournalKey> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private JournalKey idKey;

	private Date date;
	private EnumActionJournal action;

	@SuppressWarnings("unused")
	private JournalSynchronizable() {
		// TODO Auto-generated constructor stub
	}

	public JournalSynchronizable(String id, String className, Date date, EnumActionJournal action) {

		this.idKey = new JournalKey(id, className);
		this.date = date;
		this.action = action;

	}

	public JournalSynchronizable(JournalKey key, Date date, EnumActionJournal action) {

		this.idKey = key;
		this.date = date;
		this.action = action;

	}

	@Override
	@EmbeddedId
	public JournalKey getId() {
		return idKey;
	}

	@Override
	public void setId(JournalKey id) {
		this.idKey = id;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_action")
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Column(name = "action")
	@Enumerated(EnumType.STRING)
	public EnumActionJournal getAction() {
		return action;
	}

	public void setAction(EnumActionJournal action) {
		this.action = action;
	}

	@Embeddable
	public static class JournalKey implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private String id;
		private String className;

		protected JournalKey() {

		}

		public JournalKey(String id, String className) {
			this.id = id;
			this.className = className;
		}

		@Column(name = "id")
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		@Column(name = "class_name")
		public String getClassName() {
			return className;
		}

		public void setClassName(String className) {
			this.className = className;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((className == null) ? 0 : className.hashCode());
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			JournalKey other = (JournalKey) obj;
			if (className == null) {
				if (other.className != null)
					return false;
			} else if (!className.equals(other.className))
				return false;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
		}

	}

}
