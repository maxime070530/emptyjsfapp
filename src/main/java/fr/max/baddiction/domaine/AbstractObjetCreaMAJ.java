
package fr.max.baddiction.domaine;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author kcamroux
 */
@MappedSuperclass
@EntityListeners(HibernateListener.class)
public abstract class AbstractObjetCreaMAJ<T> extends AbstractId<T> {

}
