
package fr.max.baddiction.domaine;

import java.util.Date;

public interface IUpdatableSynchronize {

	Date getDateMaj();

	void setDateMaj(Date date);

	void setConvertId(String id);

}
