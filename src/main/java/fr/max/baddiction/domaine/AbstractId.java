
package fr.max.baddiction.domaine;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

/**
 * @author kcamroux
 */
@MappedSuperclass
public abstract class AbstractId<T> implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = -7817137025734545279L;

	protected T id;

	/**
	 * @return the id Integer
	 */
	@Transient
	public abstract T getId();

	public void setId(final T nId) {
		this.id = nId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		// CHECKSTYLE:OFF
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		// CHECKSTYLE:ON
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		// CHECKSTYLE:OFF
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AbstractId<?> other = (AbstractId<?>) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		// CHECKSTYLE:ON
		return true;
	}

}
