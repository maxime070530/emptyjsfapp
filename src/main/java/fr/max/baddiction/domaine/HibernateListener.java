
package fr.max.baddiction.domaine;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

/**
 * @author kcamroux
 */
public class HibernateListener {

	/**
	 * @param object AbstractObjetCreaMAJ
	 */
	@PrePersist
	public void prePersist(final AbstractObjetCreaMAJ<?> object) {

		updateObjectCreaMAJ(object);

	}

	@PreUpdate
	public void preUpdate(final AbstractObjetCreaMAJ<?> object) {

		updateObjectCreaMAJ(object);

	}

	private void updateObjectCreaMAJ(AbstractObjetCreaMAJ<?> object) {

		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (authentication != null && authentication.getPrincipal() instanceof User) {

			final User user = (User) authentication.getPrincipal();

			// USER CREA
			if (object.getUserCrea() == null) {
				object.setUserCrea(user.getUsername());
			}

			// DATE CREA
			if (object.getDateCrea() == null) {
				object.setDateCrea(new Date());
			}

			// DATE MAJ
			object.setDateMaj(new Date());
			// USER MAJ
			object.setUserMaj(user.getUsername());

		}
	}

}
