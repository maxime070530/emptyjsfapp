
package fr.max.baddiction.core;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyIntegrator {

	@Autowired
	private HibernateEventListener hibernateEventListener;

	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;

	@PostConstruct
	public void registerListeners() {

		SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class);
		EventListenerRegistry eventRegistry = sessionFactory.getServiceRegistry()
				.getService(EventListenerRegistry.class);

		eventRegistry.prependListeners(EventType.POST_DELETE, hibernateEventListener);
		eventRegistry.prependListeners(EventType.POST_UPDATE, hibernateEventListener);
		eventRegistry.prependListeners(EventType.POST_INSERT, hibernateEventListener);

		eventRegistry.prependListeners(EventType.POST_COLLECTION_UPDATE, hibernateEventListener);

	}

}
