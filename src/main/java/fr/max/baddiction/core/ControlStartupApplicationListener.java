
package fr.max.baddiction.core;

import java.lang.reflect.Method;

import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.stereotype.Component;

import fr.max.baddiction.domaine.IUpdatableSynchronize;

@Component
public class ControlStartupApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(ControlStartupApplicationListener.class);

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {

		LOGGER.info("ControlStartupApplicationListener : ContextRefreshedEvent");

		// ici check

		BeanDefinitionRegistry bdr = new SimpleBeanDefinitionRegistry();
		ClassPathBeanDefinitionScanner s = new ClassPathBeanDefinitionScanner(bdr);

		// si jar faire plutot avec properties package
		TypeFilter tf = new AssignableTypeFilter(IUpdatableSynchronize.class);
		s.addIncludeFilter(tf);
		s.setIncludeAnnotationConfig(false);
		s.scan(IUpdatableSynchronize.class.getPackage().getName());

		for (String currentBeanName : bdr.getBeanDefinitionNames()) {

			try {

				// typer class ???
				Class<?> clazz = Class.forName(bdr.getBeanDefinition(currentBeanName).getBeanClassName());

				for (Method currentMethod : clazz.getDeclaredMethods()) {

					if (IUpdatableSynchronize.class.isAssignableFrom(clazz)) {

						if (currentMethod.isAnnotationPresent(OneToMany.class)) {

							if (currentMethod.isAnnotationPresent(JoinColumn.class)) {

								JoinColumn joinColumn = currentMethod.getAnnotation(JoinColumn.class);
								if (joinColumn.nullable()) {

									// error !
									LOGGER.error(clazz.getName() + " - " + currentMethod.getName() + " : "
											+ "@JoinColumn -> la properties nullable doit etre a false");
								}

							} else {
								// manque joincolumn
								LOGGER.error(clazz.getName() + " - " + currentMethod.getName() + " : "
										+ " Manque @JoinColumn");
							}

							OneToMany oneToMany = currentMethod.getAnnotation(OneToMany.class);
							if (!oneToMany.orphanRemoval()) {

								// error !

								// manque joincolumn
								LOGGER.error(clazz.getName() + " - " + currentMethod.getName() + " : "
										+ "@OneToMany -> la properties orphanRemoval doit etre a true");
							}
						}

					}
				}

				// check si error
				// generate throw exception

			} catch (NoSuchBeanDefinitionException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				// trow e
			}
		}

	}

}
