package fr.max.baddiction.core;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

import org.springframework.core.io.AbstractResource;

/**
 * @author kcamroux
 */
public class StreamAttachmentDataSource extends AbstractResource {

	// private final static Log logger =
	// LogFactory.getLog(StreamAttachmentDataSource.class);
	/** The variable outputStream. */
	private final ByteArrayOutputStream outputStream;

	/** The variable name. */
	private String name;

	/** The variable contentType. */
	private final String contentType;

	/**
	 * StreamAttachmentDataSource.
	 * 
	 * @param listMessage Set<String>
	 * @param name        String
	 * @param contentType String
	 */
	public StreamAttachmentDataSource(final Set<String> listMessage, final String name, final String contentType) {
		this.outputStream = new ByteArrayOutputStream();
		this.name = name;
		this.contentType = contentType;
		this.name = this.name + ".txt";
		for (final String message : listMessage) {
			getOutputStream().write(message.getBytes(ConstantMime.UTF8_CHARSET), 0, message.length());
		}

	}

	/**
	 * @return String getDescription
	 */
	@Override
	public final String getDescription() {
		return "Stream resource used for attachments";
	}

	/**
	 * @return InputStream getInputStream
	 * @throws IOException IOException
	 */
	@Override
	public final InputStream getInputStream() throws IOException {
		return new ByteArrayInputStream(this.outputStream.toByteArray());
	}

	/**
	 * @return contentType String
	 */
	public final String getContentType() {
		return contentType;
	}

	/**
	 * @return name String
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @return outputStream ByteArrayOutputStream
	 */
	public final ByteArrayOutputStream getOutputStream() {
		return outputStream;
	}

}
