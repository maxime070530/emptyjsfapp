
package fr.max.baddiction.core;

import java.io.Serializable;

import org.hibernate.CallbackException;
import org.hibernate.EmptyInterceptor;
import org.hibernate.Transaction;
import org.hibernate.collection.spi.PersistentCollection;
import org.hibernate.engine.transaction.internal.TransactionImpl;
import org.hibernate.type.Type;
import org.springframework.stereotype.Component;

@Component
public class MyInterceptor extends EmptyInterceptor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// @Autowired
	// private IGenericDAO genericDAO;

	@Override
	public void onDelete(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {

		// genericDAO.toString();
		System.out.println("On delete " + entity.getClass().getSimpleName());
	}

	@Override
	public void onCollectionUpdate(Object collection, Serializable key) throws CallbackException {

		System.out.println("onCollectionUpdate");

		((PersistentCollection) collection).getOwner();
	}

	// @Override
	// public void preFlush(Iterator entities) {
	//
	// while (entities.hasNext()) {
	//
	// Object entitie = entities.next();
	// System.out.println(entitie);
	// }
	//
	// System.out.println(entities);
	// }
	//
	// @Override
	// public void postFlush(Iterator entities) {
	//
	// while (entities.hasNext()) {
	//
	// Object entitie = entities.next();
	// System.out.println(entitie);
	// }
	//
	// System.out.println(entities);
	// }

	@Override
	public void afterTransactionBegin(Transaction tx) {
		TransactionImpl tximpl = (TransactionImpl) tx;
		System.out.println(tximpl.getStatus());
		System.out.println(tx);
	}

	@Override
	public void afterTransactionCompletion(Transaction tx) {

		TransactionImpl tximpl = (TransactionImpl) tx;
		System.out.println(tximpl.getStatus());
		System.out.println(tx);
	}

	// @Override
	// public boolean onFlushDirty(final Object pEntity, final Serializable pId,
	// final Object[] pCurrentState, final Object[] pPreviousState,
	// final String[] pPropertyNames, final Type[] pTypes)
	// throws CallbackException {
	// // there must no exception be uncaught here!
	//
	// System.out.println(pEntity);
	// return true;
	// }

	// @Override
	// public String onPrepareStatement(String sql) {
	//
	// System.out.println(sql);
	// return sql;
	// }

}
