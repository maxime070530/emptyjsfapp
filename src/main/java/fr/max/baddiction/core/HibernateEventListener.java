
package fr.max.baddiction.core;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.action.spi.BeforeTransactionCompletionProcess;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.event.spi.AbstractEvent;
import org.hibernate.event.spi.PostCollectionUpdateEvent;
import org.hibernate.event.spi.PostCollectionUpdateEventListener;
import org.hibernate.event.spi.PostDeleteEvent;
import org.hibernate.event.spi.PostDeleteEventListener;
import org.hibernate.event.spi.PostInsertEvent;
import org.hibernate.event.spi.PostInsertEventListener;
import org.hibernate.event.spi.PostUpdateEvent;
import org.hibernate.event.spi.PostUpdateEventListener;
import org.hibernate.persister.entity.EntityPersister;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.max.baddiction.domaine.AbstractObjetCreaMAJ;
import fr.max.baddiction.domaine.IUpdatableSynchronize;
import fr.max.baddiction.domaine.JournalSynchronizable;
import fr.max.baddiction.enums.EnumActionJournal;

@Component
public class HibernateEventListener implements PostDeleteEventListener, PostInsertEventListener,
		PostUpdateEventListener, PostCollectionUpdateEventListener {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(HibernateEventListener.class);

	@PersistenceContext
	protected EntityManager em;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	private void writeActionJournal(AbstractEvent event, Object object) {

		if (object != null && object instanceof IUpdatableSynchronize && object instanceof AbstractObjetCreaMAJ) {

			EnumActionJournal action = null;

			if (event instanceof PostDeleteEvent) {
				action = EnumActionJournal.DELETE;
			} else if (event instanceof PostInsertEvent) {
				action = EnumActionJournal.INSERT;
			} else if (event instanceof PostUpdateEvent) {
				action = EnumActionJournal.UPDATE;
			} else if (event instanceof PostCollectionUpdateEvent) {
				action = EnumActionJournal.UPDATE;
			}

			// re essayer avec evenement postdelete
			AbstractObjetCreaMAJ<?> entity = (AbstractObjetCreaMAJ<?>) object;

			if (entity.getId() != null) {

				final JournalSynchronizable journal = new JournalSynchronizable(entity.getId().toString(),
						entity.getClass().getSimpleName(), new Date(), action);

				event.getSession().getActionQueue().registerProcess(new BeforeTransactionCompletionProcess() {

					@Override
					public void doBeforeTransactionCompletion(SessionImplementor session) {

						if (session.find(JournalSynchronizable.class, journal.getId()) == null) {

							session.persist(journal);

						} else {
							session.merge(journal);
						}

						session.flush();

					}
				});

			}
		}

	}

	@Override
	public void onPostUpdate(PostUpdateEvent event) {

		writeActionJournal(event, event.getEntity());
	}

	@Override
	public void onPostDelete(PostDeleteEvent event) {
		writeActionJournal(event, event.getEntity());
	}

	@Override
	public void onPostInsert(PostInsertEvent event) {
		writeActionJournal(event, event.getEntity());

	}

	@Override
	public void onPostUpdateCollection(PostCollectionUpdateEvent event) {
		writeActionJournal(event, event.getAffectedOwnerOrNull());
	}

	@Override
	public boolean requiresPostCommitHanding(EntityPersister persister) {
		// TODO Auto-generated method stub
		return false;
	}

	// for (Map.Entry<PersistentCollection, CollectionEntry> me : IdentityMap
	// .concurrentEntries(
	// (Map<PersistentCollection, CollectionEntry>) event
	// .getSession().getPersistenceContext()
	// .getCollectionEntries())) {
	// PersistentCollection coll = me.getKey();
	// CollectionEntry ce = me.getValue();
	//
	// if (ce.isDoupdate()) {
	// LOGGER.info("\n====collection changed==========");
	// LOGGER.info("nNewCollection\n " + coll);
	// LOGGER.info("nOldCollection" + coll.getStoredSnapshot()
	// + " ============");
	// }
	// }

}
