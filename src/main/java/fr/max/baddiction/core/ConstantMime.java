
package fr.max.baddiction.core;

import java.nio.charset.Charset;

/**
 * The Class ConstantMime.
 */
public final class ConstantMime {

	/** The Constant ZIP_MIME_TYPE. */
	public static final String ZIP_MIME_TYPE = "application/zip";

	/** The Constant PDF_MIME_TYPE. */
	public static final String PDF_MIME_TYPE = "application/pdf";

	/** The Constant CSV_MIME_TYPE. */
	public static final String CSV_MIME_TYPE = "application/csv";

	/** The Constant TEXT_MIME_TYPE. */
	public static final String TEXT_MIME_TYPE = "text/plain";

	/** The Constant MSWORD_MIME_TYPE. */
	public static final String MSWORD_MIME_TYPE = "application/msword";

	/** The Constant DEFAULT_CHARSET. */
	public static final String DEFAULT_CHARSET_MESSAGE = "UTF-8";

	/** The Constant UTF8_CHARSET. */
	public static final Charset UTF8_CHARSET = Charset.forName(DEFAULT_CHARSET_MESSAGE);

	/** The Constant ISO_8859_1. */
	private static final String ISO_8859_1 = "ISO-8859-1";

	/** The Constant ISO_8859_1_CHARSET. */
	public static final Charset ISO_8859_1_CHARSET = Charset.forName(ISO_8859_1);

	/**
	 * Instantiates a new constant.
	 */
	private ConstantMime() {
		super();
	}
}
