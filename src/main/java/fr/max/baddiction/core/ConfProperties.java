
package fr.max.baddiction.core;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author kcamroux
 */
public final class ConfProperties {

	/** The Constant BUNDLE_NAME. */
	private static final String BUNDLE_NAME = "conf";

	/** The Constant MESSAGE_INTROUVABLE. */
	private static final String MESSAGE_INTROUVABLE = "Le message est introuvable dans le fichier de resources : ";

	/**
	 * CONSTANTE MESSAGE COMMUN.
	 */

	/** The Constant TIME_PURGE_TOURNEE. */
	public static final String TIME_PURGE_TOURNEE = "time.purge.tournee";

	/** The Constant TIME_TOURNEE_OLD. */
	public static final String TIME_TOURNEE_OLD = "time.tournee.old";

	/** The Constant COMMENT_PREDEF. */
	public static final String COMMENT_PREDEF = "comment.predef";

	/** The Constant ENVIRONEMENT. */
	public static final String ENVIRONEMENT = "environement";

	/** The Constant SIMPLE_MAIL_MESSAGE_FROM. */
	public static final String SIMPLE_MAIL_MESSAGE_FROM = "simpleMailMessage.from";

	/*** FIN CONSTANTES. **/

	/**
	 * Constructeur.
	 */
	private ConfProperties() {
	}

	/**
	 * @param key    String
	 * @param locale Locale
	 * @param params Object...
	 * @return String buildMessage
	 */
	public static String buildMessage(final String key, final Locale locale, final Object... params) {

		return buildMessageConstruct(key, locale, params);
	}

	/**
	 * @param key    String
	 * @param params Object...
	 * @return String buildMessage
	 */
	public static String buildMessage(final String key, final Object... params) {
		return buildMessageConstruct(key, Locale.FRENCH, params);
	}

	/**
	 * @param key    String
	 * @param locale Locale
	 * @param params Object...
	 * @return String buildMessage
	 */
	private static String buildMessageConstruct(final String key, final Locale locale, final Object... params) {

		final ResourceBundle resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME, locale);

		if (resourceBundle == null) {
			return "Le fichier properties des messages est null";
		}
		if (key != null && !key.isEmpty()) {
			try {
				final String messagekey = resourceBundle.getString(key);
				if (params != null && params.length != 0) {
					return MessageFormat.format(messagekey, params);
				} else {

					return messagekey;
					// return new String(
					// messagekey.getBytes(ConstantMime.iso88591charset),
					// Charset.forName("UTF-8"));
				}
			} catch (final MissingResourceException e) {
				return MESSAGE_INTROUVABLE + " pour le code : " + key;
			}
		}
		return null;
	}

	/**
	 * Message.
	 * 
	 * @param key String
	 * @return String
	 */
	public static String buildMessage(final String key) {
		return buildMessage(key, new Object[] {});
	}

}
