
package fr.max.baddiction.core;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@Service
public class BeanApplicationContextAware implements ApplicationContextAware {

	private static ApplicationContext context;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		BeanApplicationContextAware.context = applicationContext;

	}

	public static <T> T getBean(Class<T> beanClass) {
		return context.getBean(beanClass);

	}

	public static Object getBean(String beanName) {
		return context.getBean(beanName);

	}

	public static String[] getBeanDefinitionNames() {
		return context.getBeanDefinitionNames();
	}

}
