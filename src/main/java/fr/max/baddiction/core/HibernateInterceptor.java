
package fr.max.baddiction.core;

import java.io.Serializable;

import org.hibernate.EmptyInterceptor;
import org.hibernate.Transaction;
import org.hibernate.type.Type;

public class HibernateInterceptor extends EmptyInterceptor {

	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {

		// if (entity instanceof IUpdatableSynchronize) {
		// // logger.info(((User) entity).toString());
		//
		// ModelDocumentPOJO model = new ModelDocumentPOJO();
		//
		// super.onSave(entity, id, state, propertyNames, types);
		// }
		// return super.onSave(entity, id, state, propertyNames, types);

		return true;
	}

	@Override
	public void beforeTransactionCompletion(Transaction tx) {
		//
		// tx.
	}
}
