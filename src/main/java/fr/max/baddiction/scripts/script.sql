

 CREATE TABLE public.club (
 	id_club SERIAL PRIMARY KEY,
 	nom_club varchar(100) NOT NULL,
 	ville_club varchar(100) NULL,
 	abrev_club varchar(100) NULL
 )
 WITH (
 	OIDS=FALSE
 ) ;


CREATE TABLE public.joueur (
	id_joueur SERIAL PRIMARY KEY,
	nom varchar(100) NOT NULL,
	age int4 NOT NULL,
	sexe varchar(100) NOT NULL,
	licence varchar(100) NOT NULL,
	cote_simple int4 NULL,
	cote_double int4 NULL,
	cote_mixte int4 NULL,
	classement_mixte varchar NULL,
	classement_simple varchar NULL,
	classement_double varchar NULL,
	externe int4 NULL,
	tournoi_paye int4 NULL,
	tournoi_gardois_paye int4 NULL
)
WITH (
	OIDS=FALSE
) ;



CREATE TABLE public.tournoi (
	id_tournoi SERIAL PRIMARY KEY,
	libelle_tournoi varchar(100) NULL,
	date_tournoi DATE NOT NULL,
	date_limite_inscription DATE NOT NULL,
	nombre_jour int4 NOT NULL,
	prix_un_tableau int4 NOT NULL,
	prix_deux_tableau int4 NULL,
	prix_trois_tableau int4 NULL,
	lien varchar(100) NULL,
	lieu_tournoi varchar(100) NOT NULL,
	detail varchar(10000000) NULL,
	oid_club int4 NULL,
	senior int4 NULL,
	discipline_simple int4 NOT NULL,
	discipline_double int4 NOT NULL,
	discipline_mixte int4 NOT NULL,
	N int4 NULL,
	R int4 NULL,
	D int4 NULL,
	P int4 NULL,
	tour_gardois int4 NULL,
	isActif int4 NULL,
	CONSTRAINT tournoi_club_fk FOREIGN KEY (oid_club) REFERENCES public.club(id_club)
	
	
)
WITH (
	OIDS=FALSE
) ;

-- table jointure -- 
CREATE TABLE public.inscription (
	id_inscription SERIAL PRIMARY KEY,
	oid_tournoi int4 NULL,
	oid_joueur int4 NULL,
	categorie_simple int4 NULL,
	categorie_double int4 NULL,
	categorie_mixte int4 NULL,
	partenaire_double int4 NULL,
	partenaire_mixte int4 NULL,
	classement_simple int4 NULL,
	classement_double int4 NULL,
	classement_mixte int4 NULL,
	date_inscription date NOT NULL,
	paye int4 NOT NULL,
	prix_inscription int4 NOT NULL,
	CONSTRAINT inscription_joueur_fk FOREIGN KEY (oid_joueur) REFERENCES public.joueur(id_joueur),
	CONSTRAINT inscription_tournoi_fk FOREIGN KEY (oid_tournoi) REFERENCES public.tournoi(id_tournoi),
	CONSTRAINT inscription_partd_fk FOREIGN KEY (partenaire_double) REFERENCES public.joueur(id_joueur),
	CONSTRAINT inscription_partm_fk FOREIGN KEY (partenaire_mixte) REFERENCES public.joueur(id_joueur)
)
WITH (
	OIDS=FALSE
) ;




CREATE TABLE util (
	id_user SERIAL PRIMARY KEY,
	nom varchar(100) NOT NULL,
	password varchar(100) NOT NULL,
	oid_joueur int4 NOT NULL,
	oid_club int4 NOT NULL,
	CONSTRAINT user_fk FOREIGN KEY (oid_joueur) REFERENCES public.joueur(id_joueur),
	CONSTRAINT club_fk FOREIGN KEY (oid_club) REFERENCES public.club(id_club)
)
WITH (
	OIDS=FALSE
) ;

CREATE TABLE public."role" (
	id_role SERIAL PRIMARY KEY,
	libelle varchar(100) NOT NULL,
	code varchar(100) NOT NULL
)
WITH (
	OIDS=FALSE
) ;


CREATE TABLE public.role_user (
	id_role_user SERIAL PRIMARY KEY,
	oid_role int4 NOT NULL,
	oid_user int4 NOT NULL,
	CONSTRAINT role_fk FOREIGN KEY (oid_role) REFERENCES public."role"(id_role),
	CONSTRAINT user_fk FOREIGN KEY (oid_user) REFERENCES util(id_user)
)
WITH (
	OIDS=FALSE
) ;



INSERT INTO public.role
(id_role, libelle, code)
VALUES (1, 'Administrateur', 'ADMI');

INSERT INTO public.role
(id_role, libelle, code)
VALUES (2, 'Joueur', 'JOU');

INSERT INTO public.role
(id_role, libelle, code)
VALUES (3, 'Visiteur', 'VIS');

INSERT INTO public.role
(id_role, libelle, code)
VALUES (4, 'Comptable', 'COMPTA');

INSERT INTO public.role
(id_role, libelle, code)
VALUES (5, 'Gestionnaire Tournoi', 'TOURN');

ALTER TABLE club add column date_crea timestamp NOT NULL default '2019-01-01 00:00:00';
ALTER TABLE club add column date_maj timestamp NOT NULL default '2019-01-01 00:00:00';
ALTER TABLE club add column user_crea varchar(255) NOT NULL default 'null';
ALTER TABLE club add column user_maj varchar(255) NOT NULL default 'IFA';

ALTER TABLE inscription add column date_crea timestamp NOT NULL default '2019-01-01 00:00:00';
ALTER TABLE inscription add column date_maj timestamp NOT NULL default '2019-01-01 00:00:00';
ALTER TABLE inscription add column user_crea varchar(255) NOT NULL default 'null';
ALTER TABLE inscription add column user_maj varchar(255) NOT NULL default 'IFA';

ALTER TABLE joueur add column date_crea timestamp NOT NULL default '2019-01-01 00:00:00';
ALTER TABLE joueur add column date_maj timestamp NOT NULL default '2019-01-01 00:00:00';
ALTER TABLE joueur add column user_crea varchar(255) NOT NULL default 'null';
ALTER TABLE joueur add column user_maj varchar(255) NOT NULL default 'IFA';

ALTER TABLE role add column date_crea timestamp NOT NULL default '2019-01-01 00:00:00';
ALTER TABLE role add column date_maj timestamp NOT NULL default '2019-01-01 00:00:00';
ALTER TABLE role add column user_crea varchar(255) NOT NULL default 'null';
ALTER TABLE role add column user_maj varchar(255) NOT NULL default 'IFA';

ALTER TABLE tournoi add column date_crea timestamp NOT NULL default '2019-01-01 00:00:00';
ALTER TABLE tournoi add column date_maj timestamp NOT NULL default '2019-01-01 00:00:00';
ALTER TABLE tournoi add column user_crea varchar(255) NOT NULL default 'null';
ALTER TABLE tournoi add column user_maj varchar(255) NOT NULL default 'IFA';

ALTER TABLE util add column date_crea timestamp NOT NULL default '2019-01-01 00:00:00';
ALTER TABLE util add column date_maj timestamp NOT NULL default '2019-01-01 00:00:00';
ALTER TABLE util add column user_crea varchar(255) NOT NULL default 'null';
ALTER TABLE util add column user_maj varchar(255) NOT NULL default 'IFA';










