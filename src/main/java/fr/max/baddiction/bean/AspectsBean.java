
package fr.max.baddiction.bean;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.primefaces.PrimeFaces;

import fr.max.baddiction.exception.ValidationException;
import fr.max.baddiction.exception.ValidationWarning;

@Aspect
public class AspectsBean {

	private static final Logger LOGGER = LogManager.getLogger(AspectsBean.class);

	@SuppressWarnings("deprecation")
	@Around(value = "execution(* fr.max.lvdc.bean.*.*.*(..))")
	public Object around(final ProceedingJoinPoint pjp) throws Throwable {

		try {
			return pjp.proceed();

		} catch (ValidationWarning vw) {

			PrimeFaces.current().ajax().addCallbackParam("validationFailed", true);

			// ici faire FACES MESSAGE WARNING

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Dans catch ValidationWarning aspect bean : execution(* fr.max.baddiction.bean.*.*.*(..))", vw);
			}
			final List<String> errors = vw.getFeedBack().getListMessages();
			if (!errors.isEmpty()) {

				for (final String current : errors) {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_WARN, current, ""));
				}
			}
		} catch (ValidationException ve) {
			// ici faire FACES MESSAGE EXCEPTION

			PrimeFaces.current().ajax().addCallbackParam("validationFailed", true);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Dans catch ValidationException aspect bean : execution(* fr.max.baddiction.bean.*.*.*(..))",
						ve);
			}
			final List<String> errors = ve.getFeedBack().getListMessages();
			if (!errors.isEmpty()) {

				for (final String current : errors) {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR, current, ""));
				}
			}
		}

		return null;

	}

}
