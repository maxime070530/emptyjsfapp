
package fr.max.baddiction.bean;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("beanIndex")
@Scope("view")
@Lazy(true)
public class BeanIndex extends AbstractGenericBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4072742364411257240L;

	// debut injection


	// fin injection

	private Integer amount = 5;

	@PostConstruct
	public void init() {
	}

}
