package fr.max.baddiction.exception;

import fr.max.baddiction.dto.commun.FeedBackExceptionDTO;

public class ValidationWarning extends ValidationException {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructeur.
	 * 
	 * @param feedBack
	 *            FeedBackExceptionDTO
	 */
	public ValidationWarning(final FeedBackExceptionDTO feedBack) {
		super(feedBack);
	}

	/**
	 * Constructeur.
	 * 
	 * @param feedBack
	 *            FeedBackExceptionDTO
	 * @param throwable
	 *            Throwable
	 */
	public ValidationWarning(final FeedBackExceptionDTO feedBack,
			final Throwable throwable) {
		super(feedBack, throwable);
	}

}
